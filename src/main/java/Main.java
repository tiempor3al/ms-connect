import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import dao.EventsDao;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import transformers.JsonTransformer;
import utils.DB;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static spark.Spark.*;

public class Main {

    private final static Logger logger = LoggerFactory.getLogger(Main.class);
    private static Properties serverProperties;


    public static void main(String[] args) throws IOException {

        final String jdbiPath = System.getenv("MSCONNECT-DB-PATH");
        final String secret = System.getenv("MSCONNECT-SECRET");

        serverProperties = new Properties();
        InputStream input = new FileInputStream(jdbiPath);
        serverProperties.load(input);

        port(8080);

        before("/api/v1/*", (request, response) -> {

            Object header = request.headers("Authorization");

            if (header == null) {
                halt(401, "Go away!");
                return;
            }

            String mydata = header.toString();
            Pattern pattern = Pattern.compile("Bearer (.*)");
            Matcher matcher = pattern.matcher(mydata);

            if (!matcher.find()) {
                halt(401, "Malformed authorization header");
                return;
            }

            String token = matcher.group(1);

            try {

                Algorithm algorithm = Algorithm.HMAC256(secret);
                JWTVerifier verifier = JWT.require(algorithm).build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);

                Claim claim = jwt.getClaim("userId");
                if(claim.isNull()){
                    halt(401, "Missing userId claim");
                    return;
                }
                request.attribute("userId", claim.asString());

            } catch (UnsupportedEncodingException exception) {
                halt(401, "UnsupportedEncodingException");
            } catch (JWTVerificationException exception) {
                halt(401, "Go away!");
            }


        });


        get("/", (req, res) -> "API v 1.01");

        get("/api/v1/events", (req, res) -> {

            res.type("application/json");
            Jdbi jdbi = DB.getJdbi(serverProperties);
            return jdbi.withExtension(EventsDao.class, dao -> dao.listEvents());

        }, new JsonTransformer());


        post("/api/v1/events", (req, res) -> {
            try {

                final String userId = req.attribute("userId");
                final String data = req.body();

                Jdbi jdbi = DB.getJdbi(serverProperties);
                jdbi.withExtension(EventsDao.class, dao -> {
                    dao.insert(userId, data);
                    return true;
                });

            } catch (Exception e) {
                throw e;
            }

            return "ok";

        }, new JsonTransformer());


        exception(Exception.class, (e, request, response) -> {
            response.status(500);
            response.body(e.getMessage());
        });

    }



}
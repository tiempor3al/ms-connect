package dao;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import vo.Event;

import java.util.List;


public interface EventsDao {


    @RegisterBeanMapper(Event.class)
    @SqlQuery("SELECT * FROM events")
    List<Event> listEvents();


    @SqlUpdate("INSERT INTO events (id, user_id, data) values (0, :user_id, :data )")
    void insert(@Bind("user_id") String userId, @Bind("data") String data );

}

package utils;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import java.io.IOException;
import java.util.Properties;

public class DB {

    public static Jdbi getJdbi(Properties prop) throws IOException {
        Jdbi jdbi = Jdbi.create(prop.getProperty("mysql.url"), prop.getProperty("mysql.username"), prop.getProperty("mysql.password"));
        jdbi.installPlugin(new SqlObjectPlugin());
        return jdbi;
    }
}
